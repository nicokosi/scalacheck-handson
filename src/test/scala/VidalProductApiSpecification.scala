import java.net.{URLEncoder, URL}

import org.scalacheck.Prop.forAll
import org.scalacheck.{Gen, Properties}
import uk.co.bigbeeconsultants.http.HttpClient

object VidalProductApiSpecification extends Properties("VidalProductApiSpecification") {

  val httpClient = new HttpClient
  val REST_URL_PREFIX = "http://apirest-dev.vidal.fr/rest"
  val AUTH_SUFFIX = ""

  val productIdGen = Gen.choose(0, 1000000)
  property("fetching product by ID should return SUCCESS or NOT_FOUND") = forAll(productIdGen){id =>
    val response = httpResponse(s"$REST_URL_PREFIX/api/product/$id?$AUTH_SUFFIX")
    response.status.isSuccess || response.status.code == 404
  }

  import org.scalacheck.Prop.BooleanOperators
  property("searching product should return SUCCESS") = forAll { query: String =>
    (query.length > 2) ==> {
      val response = httpResponse(s"$REST_URL_PREFIX/api/products?q=${encode(query)}&$AUTH_SUFFIX")
      response.status.isSuccess
    }
  }

  def encode(s: String) = URLEncoder.encode(s, "UTF-8")

  def httpResponse(url: String) = httpClient.get(new URL(url))
}
