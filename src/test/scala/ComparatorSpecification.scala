import java.util.Comparator

import com.vidal.merlin.bean.Asmr
import com.vidal.merlin.constant.AsmrDegree
import org.scalacheck.Arbitrary.arbitrary
import org.scalacheck.Prop.{BooleanOperators, forAll}
import org.scalacheck.{Arbitrary, Gen, Properties}

object ComparatorSpecification extends Properties("Comparator Specification") {

  property("symmetry") = forAll { (a: T, b: T) =>
    comparator.compare(a, b) == -comparator.compare(b, a)
  }


  property("transitivity") = forAll { (x: T, y: T, z: T) =>
    (comparator.compare(x, y) > 0 && comparator.compare(y, z) > 0) ==> {
      comparator.compare(x, z) > 0
    }
  }

  property("commutativity") = forAll { (x: T, y: T, z: T) =>
    (comparator.compare(x, y) == 0) ==> {
      sgn(comparator.compare(x, z)) == sgn(comparator.compare(y, z))
    }
  }

  val sgn = (a: Int) => a >= 0

  /** The comparator we want to test */
  private val comparator = com.vidal.merlin.bean.Asmr.ASMR_DEGREE_COMPARATOR

  type T = Asmr
  /** A generator of objects we want to compare */
  lazy val generator: Gen[T] = for {
    id <- arbitrary[Int]
    degreeIndex <- Gen.chooseNum(0, 5)
  } yield {
      val v = new Asmr
      v.setId(id)
      v.setDegree(AsmrDegree.values().array(degreeIndex))
      v
    }
  implicit lazy val arbConsumer: Arbitrary[T] = Arbitrary(generator)
}
