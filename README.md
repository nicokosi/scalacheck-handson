A `Scala` sandbox to use and **discover property-based testing** (aka generative testing) via `scalacheck` library (https://www.scalacheck.org/).

### How do I get set up? ###

1. install sbt (http://www.scala-sbt.org/)
2. run ```sbt test```
3. adapt/add tests and `goto 2`. :)