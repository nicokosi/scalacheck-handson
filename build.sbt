name := "scalacheck-handson"

version := "1.0"

scalaVersion := "2.11.6"

libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.12.2" % "test"

resolvers += "Big Bee Consultants" at "http://repo.bigbeeconsultants.co.uk/repo"

libraryDependencies += "uk.co.bigbeeconsultants" %% "bee-client" % "0.28.+"

libraryDependencies += "org.slf4j" % "slf4j-api" % "1.7.+"

libraryDependencies += "ch.qos.logback" % "logback-core"    % "1.0.+"

libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.0.+"

libraryDependencies += "org.apache.httpcomponents" % "httpclient" % "4.4.1"

resolvers += Resolver.mavenLocal

libraryDependencies += "com.vidal.merlin" % "merlin-entity" % "2015.06.0-secu-SNAPSHOT" exclude("org.codehaus.xfire", "xfire-core")